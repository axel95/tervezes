# Szakdolgozat - Szilágyi Alex

## Flutter beüzemelése Windows rendszerre.

1. IDE telepítése (Android studio, IntelliJ, Visual Studio Code)
2. Flutter SDK letöltése. A legutolsó stabil verzió letölthető [ITT](https://storage.googleapis.com/flutter_infra/releases/stable/windows/flutter_windows_v1.9.1+hotfix.2-stable.zip)

 	* A kicsomagolást olyan helyre végezzük, ami könnyen hozzáférhető, és nem igényel külön engedélyeket (pl. Users/Documents). A C//src/flutter megfelel

3. Környezeti változó megváltoztatása, hogy parancssorból is elérhető legyen a flutter command.

	* A start menü keresőjében a "Környezeti változók" kifejezésre keressünk rá
	* Itt a user tabon keressük meg a PATH bekezdést. Ha megvan, módosítsuk és adjuk hozzá az elérését a mappának, ahová korábban kicsomagoltuk a zip fájlt, és fűzzük hozzá a /bin mappát, Ha eddig nem létezett, akkor hozzuk létre a PATH környezeti változót, és adjuk hozzá a bin mappa teljes elérését.
	
4. Android Studio / IntelliJ plugin

	* File > Settings > Plugins menüpont alatt válasszuk ki a Flutter plugin, majd telepítsük a Dart nyelvhez tartozó plugint is.
	

5. Függőségek ellenőrzése

	* A `flutter doctor` parancs lefuttatásával ellenőrizhetjük, hogy minden szükséges eszköz telepítve van-e a futtatáshoz.
	
6. Restart IDE és első projekt készítése/megnyitása
