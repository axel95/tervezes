import 'package:flutter/material.dart';

class TileWidget extends StatelessWidget {
  final int boardSize;
  final int content;
  final Color color;

  TileWidget({Key key, @required this.boardSize, @required this.content, @required this.color})
      : super(key: key);

  Widget build(BuildContext context) {
    var size = (MediaQuery.of(context).size.width / boardSize) - 15;
    var cont = content;
    var textSizeMultiplier = 1.0;
    if(content > 99){
      textSizeMultiplier = 0.8;
    }

    return  Container(
        width: size ,
        height: size,
        decoration: BoxDecoration(
            color: color,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(size / 10.0))
        ),
        child: Center(
            child: Text(
              '$cont',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: textSizeMultiplier * 200.0 / boardSize,
                  color: Colors.white),
            ))
    );
  }
}
