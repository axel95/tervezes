import 'package:flutter/material.dart';
import 'package:tervezes/pages/levels_page.dart';
import 'package:tervezes/pages/profile_page.dart';
import 'package:tervezes/pages/random_page.dart';
import 'package:tervezes/pages/scores_page.dart';
import 'package:tervezes/pages/tutorial_page.dart';

class MenuButton extends StatelessWidget {
  final String _text;
  final IconData _iconData;

  MenuButton(this._text, this._iconData);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width / 5,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
        color: Colors.lightBlue,
      ),
      child: FlatButton(
        onPressed: () => navigateToPage(context, _text),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 10.0,
            ),
            Icon(
              _iconData,
              color: Colors.white,
              size: MediaQuery.of(context).size.width / 10,
            ),
            Container(
              width: 10.0,
            ),
            new Text(
              this._text,
              style: new TextStyle(
                  color: Colors.white,
                  decorationColor: Colors.black,
                  fontSize: 40.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Material'),
            ),
          ],
        ),
      ),
    );
  }

  navigateToPage(BuildContext context, String text) {
    if (text == "Levels") {
      return Navigator.of(context).push(
          MaterialPageRoute(builder: (BuildContext context) => LevelsPage()));
    } else if (text == "Random") {
      return Navigator.of(context).push(
          MaterialPageRoute(builder: (BuildContext context) => RandomPage()));
    } else if (text == "Scores") {
      return Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => ScoresPage()));
    } else if(text == "Profile") {
      return Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => ProfilePage(
          )));
    } else if(text == "Tutorial") {
      return Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => TutorialPage(
          )));
    }
  }
}