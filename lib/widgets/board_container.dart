import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tervezes/models/board_meta_model.dart';
import 'package:tervezes/models/board_model.dart';
import 'package:tervezes/models/counter_model.dart';
import 'package:tervezes/models/move_model.dart';
import 'package:tervezes/models/timer_model.dart';
import 'package:tervezes/pages/level_score_page.dart';
import 'package:tervezes/pages/score_page.dart';
import 'package:tervezes/pages/tutorial_page.dart';
import 'package:tervezes/services/animation_services.dart';
import 'package:tervezes/services/solver_service.dart';

import 'board_widget.dart';
import 'time_widget.dart';

class BoardContainer extends StatefulWidget {
  final boardSize;
  BoardContainer({Key key, @required this.boardSize});

  @override
  BoardContainerState createState() => BoardContainerState();
}

class BoardContainerState extends State<BoardContainer>
    with TickerProviderStateMixin {
  var boardSize;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    boardSize = widget.boardSize;
    _controller = AnimationController(
        duration: const Duration(milliseconds: 750), vsync: this);
  }

  Future<void> _playAnimation() async {
    try {
      await _controller.forward().orCancel;
      _controller.reset();
    } on TickerCanceled {
      // the animation got canceled
    }
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    final listOfWidgets = List<Widget>();
    final board = Provider.of<Board>(context);
    final stopwatch = Provider.of<TimerModel>(context);

    if (!board.isShuffled) {
      board.shuffle();
      board.isShuffled = true;
    }

    if (!stopwatch.isRunning) {
      stopwatch.start();
      print("Timer started!");
    }

    final gameMode = Provider.of<BoardMetaModel>(context).gameMode;

    // Variables to determine gesture initial and final positions
    var initialX, distanceX, initialY, distanceY;
    listOfWidgets.reversed;
    return Stack(children: <Widget>[
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 40.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                (gameMode == "Level" || gameMode == "Random")
                    ? TimeWidget()
                    : Container(),
                (gameMode == "Level" || gameMode == "Random")
                    ? Text(
                        "Moves: ${Provider.of<CounterModel>(context).counter}",
                        style: TextStyle(color: Colors.white, fontSize: 25.0))
                    : Container(),
              ],
            ),
          ),
          GestureDetector(
              onPanStart: (DragStartDetails details) {
                initialX = details.globalPosition.dx;
                initialY = details.globalPosition.dy;
              },
              onPanUpdate: (DragUpdateDetails details) {
                distanceX = details.globalPosition.dx - initialX;
                distanceY = details.globalPosition.dy - initialY;
              },
              onPanEnd: (DragEndDetails details) {
                final movesPerformed = Provider.of<MoveListModel>(context);

                Provider.of<CounterModel>(context).incCounter();
                if (distanceX.abs() > distanceY.abs()) {
                  // Horizontal
                  var row = ((initialY - (screenHeight - screenWidth) / 2) /
                          (screenWidth / boardSize))
                      .floor();
                  if (distanceX > 0) {
                    //Right
                    var move = MoveModel(
                        direction: "Right", index: row, size: boardSize);
                    print("Right " + row.toString());
                    board.move(move);
                    board.printBoard();
                    movesPerformed.push(move);
                  } else {
                    // Left
                    var move = MoveModel(
                        direction: "Left", index: row, size: boardSize);
                    print("Left " + row.toString());
                    board.move(move);
                    board.printBoard();
                    movesPerformed.push(move);
                  }
                } else {
                  // Vertical
                  var column = (initialX / (screenWidth / boardSize)).floor();
                  if (distanceY > 0) {
                    // Down
                    var move = MoveModel(
                        direction: "Down", index: column, size: boardSize);
                    print("Down " + column.toString());
                    board.move(move);
                    board.printBoard();
                    movesPerformed.push(move);
                  } else {
                    // Up
                    var move = MoveModel(
                        direction: "Up", index: column, size: boardSize);
                    print("Up " + column.toString());
                    board.move(move);
                    board.printBoard();
                    movesPerformed.push(move);
                  }
                }

                var solutions = Provider.of<SolutionListModel>(context);
                if (solutions.listOfMoves.length > 1 &&
                    solutions.last.direction ==
                        movesPerformed.listOfMoves.last.direction &&
                    solutions.last.index ==
                        movesPerformed.listOfMoves.last.index) {
                  solutions.listOfMoves.removeLast();
                  print("Still follows...");
                } else if (solutions.listOfMoves.length != 0 &&
                    !(solutions.last.direction ==
                            movesPerformed.listOfMoves.last.direction &&
                        solutions.last.index ==
                            movesPerformed.listOfMoves.last.index)) {
                  Provider.of<BoardMetaModel>(context).setUserFollowsHelp =
                      false;
                }

                if (board.isSolved()) {
                  stopwatch.stop();
                  final level =
                      Provider.of<BoardMetaModel>(context).levelNumber;
                  final counter = Provider.of<CounterModel>(context).counter;
                  print("Your time: ${stopwatch.currentDuration}");
                  if (gameMode == "Random") {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (context) => new ScorePage(
                            movesCounter: counter,
                            duration:
                                stopwatch.currentDuration.inMilliseconds)));
                  } else if (gameMode == "Level") {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (context) => new LevelScorePage(
                              movesCounter: counter,
                              duration: stopwatch.currentDuration
                                  .inMilliseconds, // stopwatch.currentDuration.inMilliseconds,
                              levelNumber: level,
                            )));
                  } else if (gameMode == "Tutorial") {
                    Provider.of<TutorialStageModel>(context).incTutorialStage();
                  }
                }
              },
              child: BoardWidget(boardSize: boardSize, board: board)),
          Container(
              height: 40.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.lightbulb_outline,
                    ),
                    color: Colors.yellow,
                    onPressed: () {
                      final boardMeta = Provider.of<BoardMetaModel>(context);
                      final solutions = Provider.of<SolutionListModel>(context);
                      if (solutions.listOfMoves.length == 0 ||
                          !(boardMeta.userFollowsHelp)) {
                        var solver = Solver(board.board);
                        solver.solve();
                        solutions.listOfMoves.clear();
                        solver.listOfMoves.reversed.forEach((item) {
                          solutions.push(item);
                        });

                        boardMeta.setUserFollowsHelp = true;
                      }
                      if (solutions.listOfMoves.length != 0) {
                        _playAnimation();
                      }
                    },
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                    ),
                    color: Colors.white,
                    onPressed: () {
                      var moves = Provider.of<MoveListModel>(context);
                      if (moves.listOfMoves.length != 0) {
                        board.move(moves.pop().asReversed());
                      }
                    },
                  ),
                ],
              )),
        ],
      ),
      PointerAnimation(controller: _controller.view),
    ]);
  }
}
