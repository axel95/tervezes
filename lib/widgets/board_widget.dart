import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tervezes/widgets/tile_widget.dart';

class BoardWidget extends StatelessWidget {
  final boardSize;
  final board;

  BoardWidget({Key key, @required this.boardSize, @required this.board})
      : super(key: key);

  List<Widget> generateRow(int boardSize, int rowNumber) {
    List<Widget> list = new List<Widget>();
    Widget tempWidget;
    for (int i = 0; i < boardSize; i++) {
      final content = board.getValueAtIndices(rowNumber, i);
      final color = content == boardSize * rowNumber + i
          ? Colors.lightBlue
          : Color.fromRGBO(70, 70, 70, 1.0);
      tempWidget = TileWidget(
        boardSize: boardSize,
        content: content + 1,
        color: color,
      );
      list.add(tempWidget);
    }
    return list;
  }

  List<Widget> generateTable(int boardSize) {
    List<Widget> list = new List<Widget>();
    Widget tempWidget;
    for (int i = 0; i < boardSize; i++) {
      tempWidget = new Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: this.generateRow(boardSize, i));
      list.add(tempWidget);
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    var size = (MediaQuery.of(context).size.width);
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Container(
            width: size,
            height: size,
            decoration: BoxDecoration(
                color: Color.fromRGBO(50, 50, 50, 1.0),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(Radius.circular(18.0))),
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: generateTable(boardSize)))
      ],
    );
  }
}
