import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TimeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
        "T:${(Provider.of<int>(context) ~/ 6000)}:${(Provider.of<int>(context) % 6000 / 100)}",
        style: new TextStyle(color: Colors.white, fontSize: 20.0));
  }
}
