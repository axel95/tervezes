import 'package:flutter/foundation.dart';

class CounterModel with ChangeNotifier {
  int _moveCounter;

  CounterModel(this._moveCounter);

  void incCounter() {
    _moveCounter++;
    notifyListeners();
  }

  int get counter {
    return _moveCounter;
  }
}
