import 'package:equatable/equatable.dart';

class MoveModel {
  final String direction;
  final int index;
  final int size;

  int get moveIndex {
    return index;
  }

  MoveModel({this.direction, this.index, this.size});

  MoveModel asReversed() {
    if (this.direction == "Right") {
      return MoveModel(direction: "Left", index: this.index, size: size);
    } else if (this.direction == "Left") {
      return MoveModel(direction: "Right", index: this.index, size: size);
    } else if (this.direction == "Up") {
      return MoveModel(direction: "Down", index: this.index, size: size);
    } else if (this.direction == "Down") {
      return MoveModel(direction: "Up", index: this.index, size: size);
    }
    return null;
  }

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class MoveListModel {
  MoveListModel();

  var listOfMoves = List<MoveModel>();

  MoveModel pop() {
    if (listOfMoves.isNotEmpty) {
      return listOfMoves.removeLast();
    } else
      return null;
  }

  MoveListModel.fromList(List<MoveModel> listOfMoves) {
    listOfMoves.forEach((item) => this.push(item));
  }

  void push(MoveModel move) {
    listOfMoves.add(move);
  }
}

class SolutionListModel {
  SolutionListModel();

  var listOfMoves = List<MoveModel>();

  SolutionListModel.fromList(List<MoveModel> listOfMoves) {
    listOfMoves.forEach((item) => this.push(item));
  }

  void push(MoveModel move) {
    listOfMoves.add(move);
  }

  MoveModel pop() {
    if (listOfMoves.isNotEmpty) {
      return listOfMoves.removeLast();
    } else
      return null;
  }

  MoveModel get last {
    return listOfMoves.last;
  }
}
