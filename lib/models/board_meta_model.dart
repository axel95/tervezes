import 'package:flutter/foundation.dart';

class BoardMetaModel with ChangeNotifier {
  var _levelNumber;
  var _gameMode;
  bool _userFollowsHelp;

  BoardMetaModel(this._gameMode, this._levelNumber, this._userFollowsHelp);

  bool get userFollowsHelp {
    return _userFollowsHelp;
  }

  set setUserFollowsHelp(bool param) {
    _userFollowsHelp = param;
  }

  String get gameMode {
    return _gameMode;
  }

//  set setGameMode(String param){
//    this._gameMode = param;
//  }

  int get levelNumber {
    return this._levelNumber;
  }

  set setLevelNumber(int number) {
    this._levelNumber = number;
    notifyListeners();
  }
}
