class ScoreModel {
  final userId;
  int moves;
  int time;
  int level;
  num score;

  ScoreModel({this.userId, this.level, this.score});

  factory ScoreModel.fromMap(Map data) {
    return ScoreModel(
        userId: data['userId'], level: data['level'], score: data['score']);
  }
}
