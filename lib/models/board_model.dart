import 'dart:math';
import 'package:flutter/foundation.dart';

import 'move_model.dart';

class Board with ChangeNotifier {
  final boardSize;
  List<List<int>> board = List<List<int>>();

  var isShuffled = false;

  Board(this.board, this.boardSize, this.isShuffled);

  List<List<int>> getBoard() {
    return this.board;
  }

  void printBoard() {
    print(board);
  }

  void setValueAtIndices(int i, int j, int value) {
    board[i][j] = value;
  }

  int getValueAtIndices(int i, int j) {
    return board[i][j];
  }

  void move(MoveModel moves) {
    String direction = moves.direction;
    int index = moves.index;
    int startingPoint = 0;
    int endPoint = 0;

    print("-- Move: $direction $index");

    if (direction == "Left") {
      startingPoint = 0;
      endPoint = this.boardSize - 1;

      int temp = this.getValueAtIndices(index, startingPoint);
      for (int i = startingPoint; i != endPoint; i++) {
        int value = this.getValueAtIndices(index, i + 1);
        this.setValueAtIndices(index, i, value);
      }
      this.setValueAtIndices(index, endPoint, temp);
    } else if (direction == "Right") {
      startingPoint = this.boardSize - 1;
      endPoint = 0;

      int temp = this.getValueAtIndices(index, startingPoint);
      for (int i = startingPoint; i != endPoint; i--) {
        int value = this.getValueAtIndices(index, i - 1);
        this.setValueAtIndices(index, i, value);
      }
      this.setValueAtIndices(index, endPoint, temp);
    } else if (direction == "Up") {
      startingPoint = 0;
      endPoint = this.boardSize - 1;

      int temp = this.getValueAtIndices(startingPoint, index);
      for (int i = startingPoint; i != endPoint; i++) {
        int value = this.getValueAtIndices(i + 1, index);
        this.setValueAtIndices(i, index, value);
      }
      this.setValueAtIndices(endPoint, index, temp);
    } else if (direction == "Down") {
      startingPoint = this.boardSize - 1;
      endPoint = 0;

      int temp = this.getValueAtIndices(startingPoint, index);
      for (int i = startingPoint; i != endPoint; i--) {
        int value = this.getValueAtIndices(i - 1, index);
        this.setValueAtIndices(i, index, value);
      }
      this.setValueAtIndices(endPoint, index, temp);
    }
    if (isShuffled) {
      notifyListeners();
    }
  }

  void shuffle() {
    Random random = Random();
    List<String> moves = ["Left", "Right", "Up", "Down"];
    var numberOfMoves = random.nextInt(this.boardSize * 7) + this.boardSize * 2;

    for (var i = 0; i < numberOfMoves; i++) {
      var index = random.nextInt(this.boardSize);
      var moveIndex = random.nextInt(4);
      var amountOfMovement =
          this.boardSize == 2 ? 1 : random.nextInt((this.boardSize));
      for (var j = 0; j < amountOfMovement; j++) {
        var move = MoveModel(
            direction: moves[moveIndex], index: index, size: boardSize);
        this.move(move);
      }
    }

    if (this.isSolved()) {
      this.shuffle();
    }
  }

  bool isSolved() {
    bool solved = true;
    for (int i = 0; i < this.boardSize; i++) {
      for (int j = 0; j < this.boardSize; j++) {
        if (this.getValueAtIndices(i, j) != (i * this.boardSize + j)) {
          solved = false;
        }
      }
    }
    if (solved) {
      print("SOLVED!");
    }
    return solved;
  }
}
