import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class TimerModel with ChangeNotifier {
  Stopwatch _watch;
  Timer _timer;

  Duration get currentDuration => _currentDuration;
  Duration _currentDuration = Duration.zero;

  bool get isRunning => _timer != null;

  TimerModel() {
    _watch = Stopwatch();
  }

  void start() {
    if (_timer != null) return;

    _timer = Timer.periodic(Duration(milliseconds: 10), _onTick);
    _watch.start();

    //  notifyListeners();
  }

  void stop() {
    _timer?.cancel();
    _timer = null;
    _watch.stop();
    _currentDuration = _watch.elapsed;
    // notifyListeners();
  }

  void reset() {
    stop();
    _watch.reset();
    _currentDuration = Duration.zero;

    // notifyListeners();
  }

  void _onTick(Timer timer) {
    _currentDuration = _watch.elapsed;

    // It is supposed to update the UI if the data changes,
    // but it redraws everything so it is not efficient... :(

    // notifyListeners();
  }
}
