import 'package:flutter/material.dart';
import 'board_page.dart';

class RandomPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int _boardSize = 3;
    bool leftLimitReached = false;
    bool rightLimitReached = false;
    return Material(
      color: Colors.black,
      child: InkWell(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              child: Text(
                "Tap to start!",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 40.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Material'),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Colors.lightBlue),
              child: IconButton(
                  icon: Icon(Icons.play_arrow),
                  color: Colors.white,
                  padding: EdgeInsets.all(0.0),
                  iconSize: MediaQuery.of(context).size.width / 3,
                  onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) =>
                          new BoardPage(boardSize: _boardSize)))),
            ),
            StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.chevron_left),
                        color: leftLimitReached ? Colors.grey : Colors.white,
                        padding: EdgeInsets.all(0.0),
                        iconSize: 70.0,
                        onPressed: () => setState(() {
                              if (_boardSize > 2) {
                                _boardSize--;
                                if (_boardSize == 2) {
                                  leftLimitReached = true;
                                } else if (_boardSize == 6) {
                                  rightLimitReached = false;
                                }
                              }
                            })),
                    Text("$_boardSize",
                        style: TextStyle(color: Colors.white, fontSize: 35.0)),
                    IconButton(
                        icon: Icon(Icons.chevron_right),
                        color: rightLimitReached ? Colors.grey : Colors.white,
                        padding: EdgeInsets.all(0.0),
                        iconSize: 70.0,
                        onPressed: () => setState(() {
                              if (_boardSize < 7) {
                                _boardSize++;
                                if (_boardSize == 7) {
                                  rightLimitReached = true;
                                } else if (_boardSize == 3) {
                                  leftLimitReached = false;
                                }
                              }
                            })),
                  ],
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
