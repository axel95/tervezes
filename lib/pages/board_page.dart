import 'package:flutter/material.dart';
import 'package:tervezes/models/board_meta_model.dart';
import 'package:tervezes/models/counter_model.dart';
import 'package:tervezes/models/move_model.dart';
import 'package:tervezes/models/timer_model.dart';
import 'package:tervezes/widgets/board_container.dart';
import 'package:tervezes/models/board_model.dart';
import 'package:provider/provider.dart';

class BoardPage extends StatelessWidget {
  BoardPage({Key key, @required this.boardSize}) : super(key: key);

  final board = List<List<int>>();
  final boardSize;

  @override
  Widget build(BuildContext context) {
    initializeBoard();
    return Material(
        color: Colors.black,
        child: MultiProvider(providers: [
          ChangeNotifierProvider<BoardMetaModel>(
              create: (context) => BoardMetaModel("Random", 0, true)),
          ChangeNotifierProvider<Board>(
              create: (context) => Board(board, boardSize, false)),
          ChangeNotifierProvider<CounterModel>(
              create: (context) => CounterModel(0)),
          ChangeNotifierProvider<TimerModel>(create: (context) => TimerModel()),
          Provider<MoveListModel>(
            create: (context) => MoveListModel(),
          ),
          Provider<SolutionListModel>(
            create: (context) => SolutionListModel(),
          ),
          StreamProvider<int>(
            initialData: 0,
            create: (BuildContext context) =>
                Stream.periodic(Duration(milliseconds: 10), (i) => i),
          )
        ], child: BoardContainer(boardSize: boardSize)));
  }

  void initializeBoard() {
    for (int i = 0; i < this.boardSize; i++) {
      List<int> temp = List<int>();
      for (int j = 0; j < this.boardSize; j++) {
        temp.add(j + i * boardSize);
      }
      board.add(temp);
    }
  }
}
