import 'package:flutter/material.dart';

class ScorePage extends StatelessWidget {
  final movesCounter;
  final duration;

  ScorePage({Key key, @required this.movesCounter, @required this.duration})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.black,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(
                "Congrats!\nYour results:\n $movesCounter moves \n${duration / 1000} seconds!",
                style: new TextStyle(
                    color: Colors.white,
                    fontSize: 40.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Material'),
                textAlign: TextAlign.center,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 160.0,
                    decoration: BoxDecoration(
                        color: Colors.lightBlue,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Center(
                        child: Container(
                          color: Colors.lightBlue,
                          child: Text(
                            "New Game",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25.0,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ]));
  }
}
