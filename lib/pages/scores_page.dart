import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tervezes/services/firebase_services.dart';

class ScoresPage extends StatelessWidget {
  Widget build(BuildContext context) {
    FirebaseServices fs = FirebaseServices();
    return Material(
        color: Colors.black,
        child: Container(
          child: DefaultTabController(
            length: 2,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: <
                    Widget>[
              Container(
                height: 30.0,
                color: Colors.black,
              ),
              Container(
                height: 50.0,
                child: TabBar(
                    labelColor: Colors.white,
                    indicatorSize: TabBarIndicatorSize.label,
                    tabs: [Tab(text: "Personal"), Tab(text: "Global")]),
              ),
              Container(
                height: 6.0,
              ),
              Expanded(
                child: Container(
                  color: Colors.black,
                  child: TabBarView(
                    children: [
                      FutureBuilder<QuerySnapshot>(
                          future:
                              fs.getScores(Provider.of<FirebaseUser>(context)),
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) {
                              return Center(child: CircularProgressIndicator());
                            }
                            // print("SNAP ${snapshot.data.documents}");
                            var levels = 0;
                            snapshot.data.documents.sort((m1, m2) {
                              var r = m1['levelNumber']
                                  .compareTo(m2['levelNumber']);
                              if (r != 0) return r;
                              return m1['levelNumber']
                                  .compareTo(m2['levelNumber']);
                            });
                            return Container(
                              margin: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                  color: Colors.lightBlue,
                                  border: Border.all(color: Colors.lightBlue),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20.0))),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: DataTable(
                                    //sortAscending: true,
                                    headingRowHeight: 30.0,
                                    dataRowHeight: 40.0,
                                    columns: [
                                      DataColumn(
                                        label: Text("Level",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20.0)),
                                        numeric: true,
                                      ),
                                      DataColumn(
                                        label: Text(
                                          "Score",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20.0),
                                        ),
                                        numeric: true,
                                      ),
                                    ],
                                    rows: snapshot.data.documents.map((data) {
                                      print(data.data['score']);
                                      var score = data.data['score'];
                                      var level = data.data['levelNumber'];
                                      if (data != null) {
                                        levels++;
                                        return DataRow(cells: [
                                          DataCell(Text("${level + 1}",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                              ))),
                                          DataCell(Text("$score",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                              ))),
                                        ]);
                                      } else {
                                        return DataRow(cells: [
                                          DataCell(Center(
                                              child: Text("${levels++}",
                                                  style: TextStyle(
                                                      color: Colors.white)))),
                                          DataCell(Center(
                                              child: Text("${0}",
                                                  style: TextStyle(
                                                      color: Colors.white)))),
                                        ]);
                                      }
                                    }).toList()),
                              ),
                            );
                          }),
                      GlobalScores()
                    ],
                  ),
                ),
              ),
            ]),
          ),
        ));
  }
}

class GlobalScores extends StatefulWidget {
  @override
  _GlobalScoresState createState() => _GlobalScoresState();
}

class _GlobalScoresState extends State<GlobalScores> {
  int _level = 0;

  @override
  Widget build(BuildContext context) {
    FirebaseServices fs = FirebaseServices();
    // TODO: implement build
    return FutureBuilder<QuerySnapshot>(
        future: fs.getGlobalScores(_level),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }
          var levels = 0;
          snapshot.data.documents.sort((m1, m2) {
            var r = m1['score'].compareTo(m2['score']);
            if (r != 0) return r;
            return m1['score'].compareTo(m2['score']);
          });
          var placeCounter = 0;
          return Container(
            // padding: EdgeInsets.all(10.0),
            margin: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
                color: Colors.lightBlue,
                border: Border.all(color: Colors.lightBlue),
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            child: Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 160.0,
                    // color: Colors.red,
                    child: Row(
                      children: <Widget>[
                        Text(
                          "Select level:",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                        DropdownButton<int>(
                          items: generateList(),
                          //style: TextStyle(color: Colors.white),
                          iconEnabledColor: Colors.white,
                          value: _level,
                          onChanged: (value) {
                            setState(() {
                              _level = value;
                              //placeCounter = 0;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: DataTable(
                      //sortAscending: true,
                      headingRowHeight: 30.0,
                      dataRowHeight: 40.0,
                      columns: [
                        DataColumn(
                          label: Text("#No.",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              )),
                          numeric: true,
                        ),
                        DataColumn(
                          label: Text("User",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              )),
                          numeric: true,
                        ),
                        DataColumn(
                          label: Text(
                            "Score",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          numeric: true,
                        ),
                      ],
                      rows: snapshot.data.documents.map((data) {
                        var score = data.data['score'];
                        var userId = data.reference.documentID;
                        if (data != null) {
                          levels++;
                          placeCounter++;
                          return DataRow(cells: [
                            DataCell(Text("$placeCounter",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ))),
                            DataCell(FutureBuilder<DocumentSnapshot>(
                              future: fs.getNickname(userId),
                              builder: (BuildContext context,
                                  AsyncSnapshot nickSnap) {
                                if (!nickSnap.hasData) {
                                  return Center(
                                      child: CircularProgressIndicator());
                                }

                                if (nickSnap != null &&
                                    nickSnap.hasData &&
                                    nickSnap.data.exists &&
                                    nickSnap.data.data
                                        .containsKey('nickname') &&
                                    nickSnap.data['nickname'] != null) {
                                  return Container(
                                      child:
                                          Text("${nickSnap.data['nickname']}",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                              )));
                                } else
                                  return Container(
                                      child: Text("Unknown" /*userName*/,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          )));
                              },
                            )),
                            DataCell(Center(
                                child: Text("$score",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    )))),
                          ]);
                        } else {
                          return DataRow(cells: [
                            DataCell(Center(
                                child: Text("${levels++}",
                                    style: TextStyle(color: Colors.white)))),
                            DataCell(Center(
                                child: Text("${0}",
                                    style: TextStyle(color: Colors.white)))),
                          ]);
                        }
                      }).toList()),
                ),
              ],
            ),
          );
        });
  }

  List<DropdownMenuItem<int>> generateList() {
    List<DropdownMenuItem<int>> list = List<DropdownMenuItem<int>>();
    for (int i = 0; i < 100; i++) {
      list.add(DropdownMenuItem<int>(
          child: Text("${(i + 1).toString()}"), value: i));
    }
    return list;
  }
}
