import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tervezes/models/board_meta_model.dart';
import 'package:tervezes/models/board_model.dart';
import 'package:tervezes/models/counter_model.dart';
import 'package:tervezes/models/move_model.dart';
import 'package:tervezes/models/timer_model.dart';
import 'package:tervezes/widgets/board_container.dart';
import 'package:tervezes/widgets/tile_widget.dart';

import 'level_board_page.dart';

class TutorialPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black,
      child: Container(
        child: ChangeNotifierProvider<TutorialStageModel>(
          create: (context) => TutorialStageModel(),
          child: TutorialPageContent(),
        ),
      ),
    );
  }
}

class TutorialStageModel extends ChangeNotifier {
  int _tutorialStage = 0;
  // TutorialStageModel(this._tutorialStage);

  incTutorialStage() {
    _tutorialStage++;
    notifyListeners();
  }

  int get tutorialStage {
    return _tutorialStage;
  }
}

class TutorialPageContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final stage = Provider.of<TutorialStageModel>(context);

    List<MoveModel> firstBoardSolutions = [
      MoveModel(direction: "Right", index: 2, size: 3)
    ];

    List<MoveModel> secondBoardSolutions = [
      MoveModel(direction: "Left", index: 2, size: 3),
      MoveModel(direction: "Up", index: 2, size: 3),
      MoveModel(direction: "Right", index: 2, size: 3),
      MoveModel(direction: "Down", index: 2, size: 3),
    ];

    List<Container> listOfWidgets = [
      /*TODO: First page of tutorial*/
      Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text(
              "To navigate the game you need to swipe on the board in the direction you want to perform a move",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                  color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            height: 20.0,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    } /**/,
                    child: Center(
                      child: Container(
                        child: Text(
                          "Menu",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Provider.of<TutorialStageModel>(context)
                          .incTutorialStage();
                    },
                    child: Center(
                      child: Container(
                        child: Text(
                          "Next tipp",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      )),

      /*TODO: Second page of tutorial*/
      Container(
        child: MultiProvider(providers: [
          ChangeNotifierProvider<BoardMetaModel>(
              create: (context) => BoardMetaModel("Tutorial", 0, true)),
          ChangeNotifierProvider<Board>(
              create: (context) => Board([
                    [0, 1, 2],
                    [3, 4, 5],
                    [7, 8, 6]
                  ], 3, true)),
          ChangeNotifierProvider<CounterModel>(
              create: (context) => CounterModel(0)),
          ChangeNotifierProvider<TimerModel>(create: (context) => TimerModel()),
          Provider<MoveListModel>(
            create: (context) => MoveListModel(),
          ),
          Provider<SolutionListModel>(
            create: (context) =>
                SolutionListModel.fromList(firstBoardSolutions),
          ),
//          StreamProvider<int>(
//            initialData: 0,
//            builder: (BuildContext context) =>
//                Stream.periodic(Duration(milliseconds: 10), (i) => i),
//          ),
        ], child: BoardContainer(boardSize: 3)),
      ),

      /*TODO: Third page of tutorial*/
      Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text(
              "If you need help, press the lightbulb",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                  color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            height: 20.0,
          ),
          Container(
              child: Icon(Icons.lightbulb_outline, color: Colors.yellowAccent)),
          Container(
            height: 20.0,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    } /**/,
                    child: Center(
                      child: Container(
                        child: Text(
                          "Menu",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Provider.of<TutorialStageModel>(context)
                          .incTutorialStage();
                    },
                    child: Center(
                      child: Container(
                        child: Text(
                          "Next tipp",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      )),

      /*TODO: Fourth page of tutorial*/
      Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text(
              "If you want to undo the last move, press the left arrow",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                  color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            height: 20.0,
          ),
          Container(child: Icon(Icons.arrow_back, color: Colors.white)),
          Container(
            height: 20.0,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    } /**/,
                    child: Center(
                      child: Container(
                        child: Text(
                          "Menu",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Provider.of<TutorialStageModel>(context)
                          .incTutorialStage();
                    },
                    child: Center(
                      child: Container(
                        child: Text(
                          "Next tipp",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      )),

      /*TODO: Fifth page of tutorial*/
      Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text(
              "The color of the tile will be changed once it reaches its appropriate position",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                  color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            height: 20.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TileWidget(boardSize: 4, content: 1, color: Colors.lightBlue),
              Container(
                width: 10.0,
              ),
              TileWidget(
                  boardSize: 4,
                  content: 1,
                  color: Color.fromRGBO(70, 70, 70, 1.0)),
            ],
          ),
          Container(
            height: 20.0,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    } /**/,
                    child: Center(
                      child: Container(
                        child: Text(
                          "Menu",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Provider.of<TutorialStageModel>(context)
                          .incTutorialStage();
                    },
                    child: Center(
                      child: Container(
                        child: Text(
                          "Next tipp",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      )),

      /*TODO: Sixth page of tutorial*/
      Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text(
              "There can be cases when you need to perform a switch algorithm. You can learn it on the next page by using the help",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                  color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            height: 20.0,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    } /**/,
                    child: Center(
                      child: Container(
                        child: Text(
                          "Menu",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Provider.of<TutorialStageModel>(context)
                          .incTutorialStage();
                    },
                    child: Center(
                      child: Container(
                        child: Text(
                          "Next tipp",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      )),

      /*TODO: Seventh page of tutorial*/
      Container(
        child: MultiProvider(providers: [
          ChangeNotifierProvider<BoardMetaModel>(
              create: (context) => BoardMetaModel("Tutorial", 0, true)),
          ChangeNotifierProvider<Board>(
              create: (context) => Board([
                    [0, 1, 2],
                    [3, 4, 8],
                    [6, 5, 7]
                  ], 3, true)),
          ChangeNotifierProvider<CounterModel>(
              create: (context) => CounterModel(0)),
          ChangeNotifierProvider<TimerModel>(create: (context) => TimerModel()),
          Provider<MoveListModel>(
            create: (context) => MoveListModel(),
          ),
          Provider<SolutionListModel>(
            create: (context) =>
                SolutionListModel.fromList(secondBoardSolutions),
          ),
//          StreamProvider<int>(
//            initialData: 0,
//            builder: (BuildContext context) =>
//                Stream.periodic(Duration(milliseconds: 10), (i) => i),
//          ),
        ], child: BoardContainer(boardSize: 3)),
      ),

      /*TODO: Last page of tutorial*/
      Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text(
              "You completed the tutorial!\nGood luck!",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                  color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            height: 20.0,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Center(
                      child: Container(
                        child: Text(
                          "Menu",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 160.0,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              LevelBoardPage(levelNumber: 0)));
                    },
                    child: Center(
                      child: Container(
                        child: Text(
                          "Level 1",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ))
    ];

    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width,
      child: listOfWidgets[stage.tutorialStage],
    );
  }
}
