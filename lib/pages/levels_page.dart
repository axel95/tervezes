import 'package:flutter/material.dart';
import 'package:tervezes/pages/level_board_page.dart';
import 'package:tervezes/widgets/tile_widget.dart';
import 'dart:convert';

class LevelsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black,
      child: InkWell(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              height: 20.0,
            ),
            Container(
                child: Text(
              "Select a level!",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Material'),
            )),
            Expanded(
                child: FutureBuilder(
                    future: readJson(context),
                    builder: (context, snapshot) {
                      var createdTiles = 0;
                      if (!snapshot.hasData) {
                        return Center(child: CircularProgressIndicator());
                      }
                      return ListView(
                        children: snapshot.data.map<Widget>((data) {
                          createdTiles += data.levels;
                          return Column(children: <Widget>[
                            Container(
                              height: 8.0,
                            ),
                            LevelsContainer(
                                text: data.size,
                                numberOfElements: data.levels,
                                createdTiles: createdTiles -
                                    data.levels) // Until I find a better way...
                          ]);
                        }).toList(),
                      );
                    })),
          ],
        ),
      ),
    );
  }

  readJson(var context) async {
    var layoutJson = await DefaultAssetBundle.of(context)
        .loadString("assets/level_layouts.json");
    var layoutFromJson = json.decode(layoutJson);
    var layoutList =
        layoutFromJson.map((i) => LayoutModel.fromJson(i)).toList();
    return layoutList;
  }
}

class LayoutModel {
  final size;
  final levels;

  factory LayoutModel.fromJson(Map<String, dynamic> json) {
    return LayoutModel(json['size'], json['levels']);
  }

  LayoutModel(this.size, this.levels);
}

class LevelsContainer extends StatelessWidget {
  final int text;
  final int numberOfElements;
  final int createdTiles;

  LevelsContainer(
      {Key key,
      @required this.text,
      @required this.numberOfElements,
      @required this.createdTiles})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Color.fromARGB(255, 50, 50, 50),
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      child: ExpansionTile(
          leading: Icon(Icons.arrow_drop_down_circle),
          trailing: Icon(Icons.arrow_drop_down_circle),
          title: Container(
            alignment: Alignment.center,
            child: Text(
              "$text x $text",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Material'),
            ),
          ),
          children: <Widget>[
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  makeTiles(
                      context,
                      text,
                      numberOfElements,
                      (MediaQuery.of(context).size.width / 5) - 15,
                      createdTiles)
                ]),
          ]),
    );
  }

  Widget makeTiles(BuildContext context, int text, int numberOfTiles,
      var tileSize, int createdTiles) {
    List<Widget> list = List<Widget>();
    Widget tempWidget;
    for (int i = 0; i < numberOfTiles / 5; i++) {
      tempWidget = Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: this.generateRow(context, text, i, createdTiles, tileSize));
      list.add(tempWidget);
    }

    return Container(
      height: tileSize * (numberOfTiles / 5) + 30,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: list,
      ),
    );
  }

  List<Widget> generateRow(BuildContext context, int text, int rowNumber,
      int createdTiles, var tileSize) {
    List<Widget> list = List<Widget>();
    Widget tempWidget;
    for (int i = 0; i < 5; i++) {
      final content = rowNumber * 5 + i + createdTiles;
      final color = Colors.lightBlue;
      tempWidget = Container(
        width: tileSize,
        child: FlatButton(
          padding: EdgeInsets.all(0.0),
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          onPressed: () => Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) =>
                  LevelBoardPage(levelNumber: content))),
          child: TileWidget(
            boardSize: 5,
            content: content + 1,
            color: color,
          ),
        ),
      );
      list.add(tempWidget);
    }
    return list;
  }
}
