import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:tervezes/widgets/menu_button.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = Provider.of<FirebaseUser>(context);
    if (user == null) {
      return Material(
          color: Colors.black,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  width: MediaQuery.of(context).size.width,
                  child: FlatButton(
                    onPressed: () {
                      _handleSignIn()
                          .then((FirebaseUser newUser) => {user = newUser})
                          .catchError((e) => print(e));
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          "assets/google-color.png",
                          scale: 10.5,
                        ),
                        Container(width: 10.0),
                        Text(
                          "Log In",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Material'),
                        ),
                      ],
                    ),
                  ),
                )
              ]));
    } else
      return SafeArea(
        child: new Material(
          color: Colors.black,
          child: new InkWell(
            child: Column(
              children: <Widget>[
                Container(
                  height: 40.0,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      "Menu",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 40.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Material'),
                    ),
                  ),
                ),
                Container(height: 20.0),
                Expanded(
                  child: Container(
                    child: new ListView(
                      children: <Widget>[
                        MenuButton("Tutorial", Icons.book),
                        Container(
                          height: 20.0,
                        ),
                        MenuButton("Random", Icons.shuffle),
                        Container(
                          height: 20.0,
                        ),
                        MenuButton("Levels", Icons.apps),
                        Container(
                          height: 20.0,
                        ),
                        MenuButton("Profile", Icons.account_circle),
                        Container(
                          height: 20.0,
                        ),
                        MenuButton("Scores", Icons.star)
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
  }

  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<FirebaseUser> _handleSignIn() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;
    print("signed in " + user.displayName);
    return user;
  }
}
