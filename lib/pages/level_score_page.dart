import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tervezes/models/score_model.dart';
import 'package:tervezes/services/firebase_services.dart';
import 'level_board_page.dart';

class LevelScorePage extends StatelessWidget {
  final movesCounter;
  final duration;

  final levelNumber;

  LevelScorePage(
      {Key key,
      @required this.movesCounter,
      @required this.duration,
      @required this.levelNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<FirebaseUser>(context);
    final score = movesCounter * duration / 1000;
    final scoreModel = ScoreModel(level: levelNumber, score: score);
    FirebaseServices fs = FirebaseServices();
    fs.saveScore(scoreModel, user);
    return Material(
        color: Colors.black,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(
                "Congrats ${user.displayName}!\nYou solved level $levelNumber in $movesCounter moves under ${duration / 1000} seconds!",
                style: new TextStyle(
                    color: Colors.white,
                    fontSize: 40.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Material'),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        color: Colors.lightBlue,
                      ),
                      child: FlatButton(
                        child: Text(
                          "Levels",
                          style: new TextStyle(
                              color: Colors.white,
                              fontSize: 40.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Material'),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        color: Colors.lightBlue,
                      ),
                      child: FlatButton(
                        child: Text(
                          " Next ",
                          style: new TextStyle(
                              color: Colors.white,
                              fontSize: 40.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Material'),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) => LevelBoardPage(
                                  levelNumber: levelNumber + 1)));
                        },
                      ),
                    ),
                  ])
            ]));
  }
}
