import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:tervezes/services/firebase_services.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<FirebaseUser>(context);
    FirebaseServices fs = FirebaseServices();
    return Material(
        color: Colors.black,
        child: Column(
          children: <Widget>[
            Container(
              height: 40.0,
            ),
            StreamBuilder<FirebaseUser>(
                stream: FirebaseAuth.instance.onAuthStateChanged,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Text(
                      "Not signed in",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 40.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Material'),
                    );
                  }

                  var nickName = "";
                  return FutureBuilder<DocumentSnapshot>(
                      future: fs.getNickname(user.uid),
                      builder: (context, data) {
                        if (!data.hasData || !snapshot.hasData) {
                          return Center(child: CircularProgressIndicator());
                        }
                        return Column(
                          children: <Widget>[
                            Text(
                              snapshot.hasData && data.hasData
                                  ? "Current user: ${snapshot.data.displayName}"
                                  : "Not signed in",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Material'),
                            ),
                            Text(
                              data != null &&
                                      data.hasData &&
                                      data.data.exists &&
                                      data.data.data.containsKey('nickname') &&
                                      data.data['nickname'] != null
                                  ? "Nickname: ${data.data['nickname']}"
                                  : "Nickname not set yet",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Material'),
                            ),
                            Container(
                              height: 20.0,
                            ),
                            Container(
                              padding: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.lightBlue),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0)),
                              ),
                              child: TextField(
                                style: TextStyle(color: Colors.white),
                                cursorColor: Colors.lightBlue,
                                onChanged: (text) {
                                  nickName = text;
                                  if (nickName != "" && nickName != null) {
                                    fs.setNickname(nickName, user);
                                  }
                                },
                              ),
                            ),
                            Container(
                              height: 20.0,
                            ),
                            Container(
                              height: 20.0,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.width / 5,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0)),
                                color: Colors.lightBlue,
                              ),
                              child: FlatButton(
                                onPressed: () {
                                  FirebaseAuth.instance.signOut();
                                  GoogleSignIn().signOut();
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: MediaQuery.of(context).size.width / 5,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20.0)),
                                    color: Colors.lightBlue,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        width: 10.0,
                                      ),
                                      Icon(
                                        Icons.exit_to_app,
                                        color: Colors.white,
                                        size:
                                            MediaQuery.of(context).size.width /
                                                10,
                                      ),
                                      Container(
                                        width: 10.0,
                                      ),
                                      Text(
                                        "Sign Out",
                                        style: TextStyle(
                                            color: Colors.white,
                                            decorationColor: Colors.black,
                                            fontSize: 40.0,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Material'),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        );
                      });
                }),
          ],
        ));
  }
}
