import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tervezes/models/board_meta_model.dart';
import 'package:tervezes/models/counter_model.dart';
import 'package:tervezes/models/move_model.dart';
import 'package:tervezes/models/timer_model.dart';
import 'package:tervezes/widgets/board_container.dart';
import 'package:tervezes/models/board_model.dart';
import 'package:provider/provider.dart';

class LevelBoardPage extends StatelessWidget {
  LevelBoardPage({Key key, @required this.levelNumber}) : super(key: key);

  final levelNumber;

  @override
  Widget build(BuildContext context) {
    print("Level: ${this.levelNumber}");
    return Material(
        color: Colors.black,
        child: FutureBuilder<List<List<int>>>(
            future: readBoardJson(context, levelNumber),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              }
              return MultiProvider(providers: [
                ChangeNotifierProvider<BoardMetaModel>(
                    create: (context) =>
                        BoardMetaModel("Level", levelNumber, true)),
                ChangeNotifierProvider<Board>(
                    create: (context) =>
                        Board(snapshot.data, snapshot.data.length, true)),
                ChangeNotifierProvider<CounterModel>(
                    create: (context) => CounterModel(0)),
                ChangeNotifierProvider<TimerModel>(
                    create: (context) => TimerModel()),
                Provider<MoveListModel>(
                  create: (context) => MoveListModel(),
                ),
                Provider<SolutionListModel>(
                  create: (context) => SolutionListModel(),
                ),
                StreamProvider<int>(
                  initialData: 0,
                  create: (BuildContext context) =>
                      Stream.periodic(Duration(milliseconds: 10), (i) => i),
                )
              ], child: BoardContainer(boardSize: snapshot.data.length));
            }));
  }

  Future<List<List<int>>> readBoardJson(
      BuildContext context, int levelNumber) async {
    final boardsJson =
        await DefaultAssetBundle.of(context).loadString("assets/levels.json");

    List<List<List<int>>> boardFromJson(String str) =>
        List<List<List<int>>>.from(json.decode(str).map((x) =>
            List<List<int>>.from(
                x.map((x) => List<int>.from(x.map((x) => x))))));
    final levelList = boardFromJson(boardsJson);
    return levelList[levelNumber];
  }
}
