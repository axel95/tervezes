import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tervezes/models/move_model.dart';

class PointerAnimation extends StatelessWidget {
  PointerAnimation({Key key, this.controller})
      : fadeOut = ColorTween(
          begin: Colors.transparent,
          end: Colors.yellowAccent,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.0,
              1.0,
              curve: PulseCurve(),
            ),
          ),
        ),
        super(key: key);

  final Animation<double> controller;
  final Animation<Color> fadeOut;

  RelativeRect _calculateStart(MoveModel move, double width) {
    final tileSize = width / move.size;
    final tileCenter = tileSize / 2;
    final fixPos = -width - 100;
    final indexedPos = -width + 2 * (tileCenter + (move.index * tileSize));
//    final fixPos = 0.0 ;
//    final inexedPos =0.0;
    if (move.direction == "Left") {
      return RelativeRect.fromLTRB(-fixPos, indexedPos, 0, 0);
    } else if (move.direction == "Right") {
      return RelativeRect.fromLTRB(fixPos, indexedPos, 0, 0);
    } else if (move.direction == "Up") {
      return RelativeRect.fromLTRB(indexedPos, -fixPos, 0, 0);
    } else if (move.direction == "Down") {
      return RelativeRect.fromLTRB(indexedPos, fixPos, 0, 0);
    }
  }

  RelativeRect _calculateEnd(MoveModel move, double width) {
    final tileSize = width / move.size;
    final tileCenter = tileSize / 2;
    final fixPos = -width + tileCenter;
    final inexedPos = -width + 2 * (tileCenter + (move.index * tileSize));
    if (move.direction == "Left") {
      return RelativeRect.fromLTRB(fixPos, inexedPos, 0, 0);
    } else if (move.direction == "Right") {
      return RelativeRect.fromLTRB(-fixPos, inexedPos, 0, 0);
    } else if (move.direction == "Up") {
      return RelativeRect.fromLTRB(inexedPos, fixPos, 0, 0);
    } else if (move.direction == "Down") {
      return RelativeRect.fromLTRB(inexedPos, -fixPos, 0, 0);
    } else
      return null;
  }

  // This function is called each time the controller "ticks" a new frame.
  // When it runs, all of the animation's values will have been
  // updated to reflect the controller's current value.
  Widget _buildAnimation(BuildContext context, Widget child) {
    final movesList = Provider.of<SolutionListModel>(context);

    final move = movesList.listOfMoves.length > 0
        ? movesList.listOfMoves.last
        : MoveModel(
            direction: "Left",
            index: 0,
            size: 1); /*MoveModel(direction: "Down",index: 2, size: 4);*/
    final width = MediaQuery.of(context).size.width;
    final RelativeRectTween relativeRectTween = RelativeRectTween(
      begin: _calculateStart(move, width),
      /*RelativeRect.fromLTRB( -width, -width, 0 , 0)*/
      end: _calculateEnd(move, width),
      /*RelativeRect.fromLTRB( 0, 0, -width, -width),*/
    );
    return Stack(
      children: <Widget>[
        PositionedTransition(
          rect: relativeRectTween
              .animate(controller.drive(CurveTween(curve: Curves.easeInOut))),
          child: Container(
            child: Container(
              width: 200,
              height: 200,
              padding: EdgeInsets.all(20),
              child: Icon(Icons.touch_app, color: fadeOut.value, size: 40),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      builder: _buildAnimation,
      animation: controller,
    );
  }
}

class PulseCurve extends Curve {
  @override
  double transformInternal(double t) {
    return sin(pi * t);
  }
}
