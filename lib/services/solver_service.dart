import 'package:tervezes/models/move_model.dart';

class Solver {
  final List<List<int>> _boardInit;
  var currentRowIndex;
  var currentColIndex;
  List<List<int>> board;
  var listOfMoves = List<MoveModel>();

  Solver(this._boardInit);

  bool solved = false;

  /* Copying to a completely different matrix is necessary
    for calculation without screen update */
  void copyBoard() {
    List<List<int>> boardTemp = List();
    var length = this._boardInit.length;

    for (int i = 0; i < length; i++) {
      List<int> tempList = List();
      for (int j = 0; j < length; j++) {
        tempList.add(0);
      }
      boardTemp.add(tempList);
    }

    for (int i = 0; i < length; i++) {
      for (int j = 0; j < length; j++) {
        boardTemp[i][j] = _boardInit[i][j];
      }
    }
    board = boardTemp;
  }

  ///Solves the received board
  void solve() {
    // List<List<int>> boardTemp = List();
    var length = this._boardInit.length;
    solved = false;
    copyBoard();

    for (int valueInOrder = 0;
        valueInOrder < length * (length - 1);
        valueInOrder++) {
      findIndexes(valueInOrder);
      var originalRowIndex = valueInOrder ~/ length;
      var originalColIndex = valueInOrder % length;
      print("Original: $originalRowIndex, $originalColIndex");
      var distanceRow = currentRowIndex - originalRowIndex;
      var distanceCol = currentColIndex - originalColIndex;

      /*If it is in the correct column*/
      if (distanceCol == 0 && distanceRow != 0 && originalRowIndex != 0) {
        String dir = "Right";
        int mult = 1;
        if (currentColIndex == 0) {
          dir = "Right";
        } else if (currentColIndex == (length - 1)) {
          dir = "Left";
          mult = -1;
        }
        move(MoveModel(direction: dir, index: currentRowIndex, size: length));
        distanceCol += mult;
        currentColIndex += mult;
      }

      /* If it is in the correct row */
      if (distanceRow == 0 && distanceCol != 0) {
        var rowToMove = currentRowIndex + 1;

        /*It's not in the first row*/
        if (currentRowIndex != 0 ||
            currentRowIndex == 0 && originalColIndex != 0) {
          move(MoveModel(
              direction: "Down", index: originalColIndex, size: length));
          move(MoveModel(
              direction: "Down", index: currentColIndex, size: length));
        } else if (originalColIndex == 0) {
          rowToMove = currentRowIndex;
        }

        if (distanceCol > 0) {
          for (int i = 0; i < distanceCol; i++) {
            move(MoveModel(direction: "Left", index: rowToMove, size: length));
          }
        } else if (distanceCol < 0) {
          for (int i = 0; i < distanceCol.abs(); i++) {
            move(MoveModel(direction: "Right", index: rowToMove, size: length));
          }
        }
        if (currentRowIndex != 0 || originalColIndex != 0) {
          move(MoveModel(
              direction: "Up", index: originalColIndex, size: length));
          move(
              MoveModel(direction: "Up", index: currentColIndex, size: length));
        }
      }

      if (distanceRow > 0) {
        if (originalRowIndex != 0) {
          for (int i = 0; i < distanceRow; i++) {
            print("$i");
            move(MoveModel(
                direction: "Down", index: originalColIndex, size: length));
          }
        }

        if (distanceCol > 0) {
          for (int i = 0; i < distanceCol; i++) {
            move(MoveModel(
                direction: "Left", index: currentRowIndex, size: length));
          }
        } else if (distanceCol < 0) {
          for (int i = 0; i < distanceCol.abs(); i++) {
            print("$i");
            move(MoveModel(
                direction: "Right", index: currentRowIndex, size: length));
          }
        }
        for (int i = 0; i < distanceRow; i++) {
          move(MoveModel(
              direction: "Up", index: originalColIndex, size: length));
        }
      }
      print(board);
    }

    print("Starting last row");
    var lastColIndex = length - 1;
    var lastRowIndex = length - 1;
    var startingTile = length * (length - 1);
    var currentTile = startingTile + 1;

    findIndexes(startingTile);

    var originalRowIndex = startingTile ~/ length;
    var originalColIndex = startingTile % length;
    print("Original: $originalRowIndex, $originalColIndex");
    var distanceCol = currentColIndex - originalColIndex;
    print("Dist: $distanceCol");

    /*Move the smallest tile to the left side*/
    var dir = "Left";
    if (distanceCol < 0) {
      dir = "Right";
    }
    for (int i = 0; i < distanceCol.abs(); i++) {
      move(MoveModel(direction: dir, index: lastRowIndex, size: length));
    }

    print(board);

    /*If the board is smaller than 4, then it should be solved (last row doesn't permute)*/
    if (length < 4) {
      print("<4 ready");
      print(board);
    }

    if (length >= 4) {
      print("\n\nSolving 4+\n\n");
      print("StartingIDX: $startingTile");
      for (int i = startingTile + 1; i < length * length; i++) {
        print("\nCurrent to solve: $i");
        findIndexes(currentTile);

        var alreadyElevated = currentColIndex == lastColIndex &&
            currentRowIndex == lastRowIndex - 1;
        /*If it is in the last column*/
        if (!alreadyElevated && currentColIndex == lastColIndex) {
          print("\nIt is in the same col");
          move(
              MoveModel(direction: "Right", index: lastRowIndex, size: length));
        }
        move(MoveModel(direction: "Down", index: lastColIndex, size: length));
        if (!alreadyElevated && currentColIndex == lastColIndex) {
          move(MoveModel(direction: "Left", index: lastRowIndex, size: length));
        }
        for (int j = 0; j < (lastColIndex - currentColIndex); j++) {
          move(
              MoveModel(direction: "Right", index: lastRowIndex, size: length));
        }
        move(MoveModel(direction: "Up", index: lastColIndex, size: length));

        print("\n$currentTile should not be in the last row");
        print(board);
        /*Move back to the firs column*/
        findIndexes(startingTile);
        for (int i = 0; i < currentColIndex.abs(); i++) {
          move(MoveModel(direction: "Left", index: lastRowIndex, size: length));
        }

        print("\nTheoretically $startingTile is the first");
        print(board);
        findIndexes(startingTile);
        var currentTilePlace = currentTile % length;
        // move(MoveModel(direction: "Down", index: lastColIndex, size: length));
        for (int j = 0; j < lastColIndex - currentTilePlace; j++) {
          print("$length $startingTile $currentTile");
          move(
              MoveModel(direction: "Right", index: lastRowIndex, size: length));
        }
        move(MoveModel(direction: "Down", index: lastColIndex, size: length));

        print(board);
        /*Move back to the firs column*/
        findIndexes(startingTile);
        for (int i = 0; i < currentColIndex.abs(); i++) {
          move(MoveModel(direction: "Left", index: lastRowIndex, size: length));
        }
        move(MoveModel(direction: "Up", index: lastColIndex, size: length));
        print(board);

        /*Move back to the firs column*/
        findIndexes(startingTile);
        for (int i = 0; i < currentColIndex.abs(); i++) {
          move(MoveModel(direction: "Left", index: lastRowIndex, size: length));
        }
        if (isSolved()) {
          solved = true;
          break;
        }

        currentTile++;
      }
      print(board);
    }

    if (!isSolved()) {
      print("Still not solved ${board[lastRowIndex][lastColIndex]}");
      if (board[lastRowIndex][lastColIndex] == (length * length - 1)) {
        move(MoveModel(direction: "Right", index: lastRowIndex, size: length));
        move(MoveModel(direction: "Down", index: lastRowIndex, size: length));
        move(MoveModel(direction: "Left", index: lastRowIndex, size: length));
        move(MoveModel(direction: "Up", index: lastRowIndex, size: length));
      }
      var sideDirectionOne;
      var sideDirectionTwo;
      var mainDirection;
      if (board[lastRowIndex][lastColIndex] == (length * length - 2)) {
        print("Last row not ok");
        mainDirection = "Up";
        sideDirectionOne = "Right";
        sideDirectionTwo = "Left";
      } else if (board[lastRowIndex][lastColIndex] ==
          length * (length - 1) - 1) {
        print("Last col not ok");
        mainDirection = "Right";
        sideDirectionOne = "Down";
        sideDirectionTwo = "Up";
      }

      for (int i = 0; i < length / 2; i++) {
        move(MoveModel(
            direction: mainDirection, index: lastRowIndex, size: length));
        move(MoveModel(
            direction: sideDirectionOne, index: lastRowIndex, size: length));
        move(MoveModel(
            direction: mainDirection, index: lastRowIndex, size: length));
        move(MoveModel(
            direction: sideDirectionTwo, index: lastRowIndex, size: length));
      }
      move(MoveModel(
          direction: mainDirection, index: lastRowIndex, size: length));
    }
    print(board);
    if (isSolved()) {
      solved = true;
      print("Solved");
    }
    solved = true;
  }

  bool isSolved() {
    int length = board.length;
    for (int i = 0; i < length; i++) {
      for (int j = 0; j < length; j++) {
        if (board[i][j] != i * length + j) {
          return false;
        }
      }
    }
    return true;
  }

  void findIndexes(int value) {
    int length = board.length;
    for (int i = 0; i < length; i++) {
      for (int j = 0; j < length; j++) {
        if (board[i][j] == value) {
          currentRowIndex = i;
          currentColIndex = j;
        }
      }
    }
    print("Indexes =$currentRowIndex, $currentColIndex ");
  }

  void move(MoveModel moves) {
    String direction = moves.direction;
    int index = moves.index;
    int startingPoint = 0;
    int endPoint = 0;
    int boardSize = board.length;

    if (!isSolved()) {
      print(".${moves.direction} ${moves.index}");
      if (listOfMoves.length < 1 ||
          listOfMoves.last.index != moves.index ||
          listOfMoves.last.direction != reverseDirection(moves.direction)) {
        listOfMoves.add(moves);
      } else if (listOfMoves.last.index == moves.index &&
          listOfMoves.last.direction == reverseDirection(moves.direction)) {
        print("Removes");
        listOfMoves.removeLast();
      }

      print("-- Move: $direction $index");
      if (direction == "Left") {
        startingPoint = 0;
        endPoint = boardSize - 1;

        int temp = board[index][startingPoint];
        for (int i = startingPoint; i != endPoint; i++) {
          int value = board[index][i + 1];
          board[index][i] = value;
        }
        board[index][endPoint] = temp;
      } else if (direction == "Right") {
        startingPoint = boardSize - 1;
        endPoint = 0;

        int temp = board[index][startingPoint];
        for (int i = startingPoint; i != endPoint; i--) {
          int value = board[index][i - 1];
          board[index][i] = value;
        }
        board[index][endPoint] = temp;
      } else if (direction == "Up") {
        startingPoint = 0;
        endPoint = boardSize - 1;

        int temp = board[startingPoint][index];
        for (int i = startingPoint; i != endPoint; i++) {
          int value = board[i + 1][index];
          board[i][index] = value;
        }
        board[endPoint][index] = temp;
      } else if (direction == "Down") {
        startingPoint = boardSize - 1;
        endPoint = 0;

        int temp = board[startingPoint][index];
        for (int i = startingPoint; i != endPoint; i--) {
          int value = board[i - 1][index];
          board[i][index] = value;
        }
        board[endPoint][index] = temp;
      }
    }
    /* For testing */
    //sleep(const Duration(milliseconds: 500));
  }

  String reverseDirection(String direction) {
    if (direction == "Left") {
      return "Right";
    } else if (direction == "Right") {
      return "Left";
    } else if (direction == "Up") {
      return "Down";
    } else if (direction == "Down") {
      return "Up";
    } else
      return null;
  }
}
