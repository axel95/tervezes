import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:tervezes/models/score_model.dart';

class FirebaseServices {
  getScore(FirebaseUser userParam, String level) {
    final db = Firestore.instance;
    db
        .collection('users')
        .document(userParam.uid)
        .collection('levels')
        .getDocuments();
  }

  Future<QuerySnapshot> getScores(FirebaseUser userParam) {
    final db = Firestore.instance;
    return db
        .collection('users')
        .document(userParam.uid)
        .collection('levels')
        .getDocuments();
  }

  Future<QuerySnapshot> getGlobalScores(int level) {
    final db = Firestore.instance;
    return db
        .collection('scores')
        .document(level.toString())
        .collection('user')
        .getDocuments();
  }

  saveScore(ScoreModel score, FirebaseUser userParam) {
    final db = Firestore.instance;
    var previous;
    print(userParam.displayName);
    db
        .collection('users')
        .document(userParam.uid)
        .collection('levels')
        .document(score.level.toString())
        .get()
        .then((DocumentSnapshot snapshot) {
      if (snapshot.data != null) {
        print("Previous best: ${snapshot.data}");
        previous = snapshot.data['score'];
      } else
        previous = null;
    }).then((_) {
      if (previous == null || previous > score.score) {
        print(
            "${score.level.toString()} ${score.score} < $previous BETTER OR NULL\n");
        db
            .collection('users')
            .document(userParam.uid)
            .collection('levels')
            .document('${score.level.toString()}')
            .setData({'score': score.score, 'levelNumber': score.level});
        db
            .collection('scores')
            .document(score.level.toString())
            .collection('user')
            .document(userParam.uid)
            .setData({'score': score.score, 'user': userParam.displayName});
      }
    });
  }

  setNickname(String nickname, FirebaseUser userParam) {
    final db = Firestore.instance;
    db
        .collection('users')
        .document(userParam.uid)
        .setData({'nickname': nickname});
  }

  Future<DocumentSnapshot> getNickname(String uid) {
    final db = Firestore.instance;
    return db.collection('users').document(uid).get();
  }
}
